
.. _ref-loadmodel:

============================
Locating and loading a model
============================

All models stored on JWS Online are assigned a unique *slug* for easy identification, and all models are stored under:

    `<http://jjj.bio.vu.nl/models/>`_

For example, the yeast glycolytic model published by Teusink *et al.* (`Can yeast glycolysis be understood in terms of in vitro kinetics of the constituent enzymes? Testing biochemistry. <http://www.ncbi.nlm.nih.gov/pubmed/10951190>`_) is referred to on JWS online with the simple *teusink* slug, and thus can be accessed directly at:

    `<http://jjj.bio.vu.nl/models/teusink>`_

The `Model Database <http://jjj.bio.vu.nl/models/>`_ link in the navigation bar will take you to a table of all the models available on JWS Online.

.. image:: /_static/1_database.png
    :width: 1200px
    :align: center

Models can be located using the search criteria in the `filter box <http://jjj.bio.vu.nl/models/?id=teusink&organism=&process=&jwsmodel__model_type=>`_ on the righthand side. In this example the model list is constrained to show the models matching *teusink* in the *name* criterion:

.. image:: /_static/1_filteredlist.png
    :width: 1200px
    :align: center

.. |model_buttons| image:: /_static/1_buttons.png
    :align: middle
.. |play_button| image:: /_static/1_playbutton.png
    :align: middle

Each listed model has a set of action buttons |model_buttons|. These buttons allow the model to be downloaded in *SBML*, *Mathematica notebook*, *JWS*, and *PySCeS* format respectively. The |play_button| loads the model in the :ref:`Model Simulation page <ref-schema>`. From this page all the simulation and analysis features of JWS Online can be explored.

.. image:: /_static/1_simulate.png
    :width: 1200px
    :align: center

Uploading a model
-----------------

Models may be uploaded to JWS Online in *SBML* or *JWS* format by clicking on the `Upload model <http://jjj.bio.vu.nl/models/upload/>`_ link in the navigation bar, and selecting a file, then clicking *Upload*. Additionally, it may be specified that JWS Online attempts to *Regenerate IDs* from element names if generic IDs have been used (e.g. species1, reaction1). Finally, if the model should be treated as a *constraint-based model* (regardless of whether it actually **IS** a *constraint-based model*), this too may be specified:

.. image:: /_static/1_upload.png
