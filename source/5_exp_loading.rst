
.. _ref-loadexp:

Locating and loading a Simulation Experiment
============================================

In JWS Online all simulation experiments follow the format principle of `SED-ML Level 1 Version 2 <http://sed-ml.org/>`_, with some extension of the Level 1 Version 3 draft.
These "extensions" are in place to deal with experimental data stored in `Fairdom Seek Instance <https://fairdomhub.org/>`_. Currently only Excel-Spreadsheets are supported,
which are supposed to follow certain format guidelines.

Similar to models each simulation experiment has a *slug* assigned, which is unique among other simulations. For example figure 3A form *Division of labor by dual feedback regulators controls JAK2/STAT5 signaling over broad ligand range* published by Bachmann *et al.* is accessible under http://jjj.bio.vu.nl/models/experiments/bachmann-user/

All simulation experiments are stored in the `Simulation Database <http://jjj.bio.vu.nl/models/experiments/>`_

.. image:: /_static/5_database.png
        :align: center

.. |exp_buttons| image:: /_static/5_buttons.png
        :align: middle
.. |play_button| image:: /_static/1_playbutton.png
        :align: middle

For each simulation experiment there is a set of buttons available, allowing to download the experiment in various formats. |exp_buttons| Whereby the SED-ML export only covers the experimental setup (a single .sedml file) the `COMBINE archive <http://co.mbine.org/standards/omex>`_ also includes all models used in the simulation experiment encoded as SBML.
In contrast the play button |play_button| starts the simulation and shows the results afterwards. 

Uploading a Simulation Experiment
---------------------------------

Simulation Experiments may be uploaded to JWS Online as SED-ML file or as COMBINE archive, by switching to the *User Simulations* or *Session Simulations* tab and clicking the upload button in the *Simulation Database*.

.. image:: /_static/5_upload_button.png
        :align: center

If a standalone .sedml file is uploaded, JWS Online tries to find the referenced models in it's own database. When this fails and the model reference is an URL, JWS Online may try to download and import the model as SBML. In case none of this is successful the model remains *un-wired*, which results in a warning. However is a COMBINE archive is uploaded, JWS Online tries to find the model in the archive itself, before continuing with the former described steps.
A simulation with one or more *un-wired* models cannot be simulated.
Further you may specify an ID as well as a name when uploading a standalone .sedml file. Though these settings are omitted when uploading a COMBINE archive, due to the possibility of housing multiple Simulation Experiments. But the filename within the archive are set as ID for both imported simulations and models.

.. image:: /_static/5_upload.png
        :align: center
