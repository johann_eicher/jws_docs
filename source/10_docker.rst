.. _ref-docker:

JWS Online Docker Support
=========================

JWS Online comes with native `Docker <https://www.docker.com/>`_ support.
To run JWS Online from a Docker container a simple *docker run* is not
sufficient due to JWS Online's dependence on a PostgreSQL database. Therefore a
`Docker Compose <https://docs.docker.com/compose/>`_ script has been provided
in order to make deployment as simple as possible. Create a file with the name
*docker-compose.yml* in a new empty folder and paste the following content into it.

**It is strongly advised that you alter the database password**. Please refer
to `Environment Variables <#id1>`_ and the `compose file reference <https://docs.docker.com/compose/compose-file/>`_
for explanations of the different configuration parameters.

.. code:: yaml

        # JWS Online Docker Compose
        #
        # This is an example file with sane defaults. Nevertheless it is strongly recommended
        # that you change the database password and adjust the persistent volume for Postgres according to
        # the deployment configuration. Also you need to configure a SMTP server in order to be able
        # to send mails from this JWS Online instance.
        version: "2"
        services:
                jwsonline-db:
                        image: postgres
                        environment:
                                # database user name
                                - POSTGRES_USER=jwsonline
                                # database password
                                - POSTGRES_PASSWORD=database-pwd
                                # database name
                                - POSTGRES_DB=jwsonline
                                # data directory for postgres
                                # - PGDATA=  # directory
                        volumes:
                                # register default PGDATA as volume
                                - ./pgsql:/var/lib/postgresql
                        expose:
                                - "5432"
                        
                jwsonline-django:
                        image: jwsonline/jwsonline:latest
                        ports: 
                                - "80:8000"
                        depends_on:
                                - jwsonline-db
                        environment:
                                # database url. Path is name of the database
                                - DATABASE=pgsql://jwsonline:database-pwd@jwsonline-db:5432/jwsonline
                                # SMTP url. Use SMTPS as scheme to enable TLS
                                - SMTP=smtp://localhost
                                # enable or disable debug view
                                - DEBUG=False
                                # comma-separated list of allowed hosts. Default allows only access via localhost
                                - ALLOWED_HOSTS=*
                                # host where the Mathematica package is running. Required for simulations
                                - MATHEMATICA=https://jjj.bio.vu.nl


To run / stop / update the server execute:

.. code:: bash
        
        docker-compose up
        docker-compose down
        docker-compose pull

Note: on the first run Docker will download the JWS Online image from the Docker Hub, if it does not find it locally.
Also the very first start of PostgreSQL and JWS Online in Docker container may take a while, since both need to initially create 
data and databasestructures.

Building the JWS Online Docker image
------------------------------------

To ease the build, tag, and push process, the fabric script was extended with a Docker goal.
It automatically builds the image using the *Dockerfile* in the repository, tags the image (alos as latest) and pushes it to
the `Docker Hub <https://hub.docker.com/r/jwsonline/jwsonline/>`_. It is strongly recommended to use the *fab* script.

.. code:: bash
        
        fab docker

Environment Variables
---------------------

In order to provide an interface to different settings options, we have exposed
some of the configurable parameters as environment variables.  For an
explanation of the available environment variables for *postgres*, please refer
to the `PostgeSQL DockerHub image <https://hub.docker.com/_/postgres/>`_

**DATABASE**
        URL to the PostgreSQL database, following the common URL scheme. The scheme is required to be *pgsql*. 

        pgsql://[db-user]:[db-password]@[db-host]:[db-port]/[db-name]
**SMTP**
        URL to configure the SMTP server used for sending account registration notices, password reset requests and error messages.
        The scheme is required to be *smtp* or *smtps* for TLS.

        smtps://[smtp-user]:[smtp-password]@[smtp-host]:[smtp-port]
**DEBUG**
        If set to true JWS Online will show detailed Django Error pages, instead of *Whoops!*.
**ALLOWED_HOSTS**
        Comma separated list of hosts, to which this server will reply. A wildcard indicates, that the server will reply to requests to any hostname.
**MATHEMATICA**
        URL to the Mathematica instance, without trailing slash.

        http://localhost
**MASYMOS_ENABLE**
        If set to true this enables basic support for `MaSyMoS <https://sems.uni-rostock.de/projects/masymos/>`_ as search engine for models.
        Please be aware, that this support is **widely untested and unstable** at the moment. As default MaSyMoS is disabled.
**MASYMOS_BASE_URL**
        URL to a running MaSyMoS instance. Default is *http://localhost:7474/morre*
**LOCAL_URL**
        URL to this JWS Online instance. This needs to be reachable from the MaSyMoS database to retrieve the public models and store them in the search index.
        Default is *http://localhost:8000/*
**WEBCAT_ENABLE**
        Enables the 'View in webCAT' link in the Download dropdown menu for Simulations. (Only works for curated models)
**WEBCAT_URL**
        URL to a webCAT instance, without trailing slash. Default is the SEMS webCAT instance `http://webcat.sems.uni-rostock.de <http://webcat.sems.uni-rostock.de/>`_
