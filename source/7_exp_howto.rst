
.. _ref-exp_howto:

Create a Simulation Experiment from a model
===========================================

To create a new *Simulation Experiment* based on a model, open the model of your choice in the :ref:`time evolution view  <ref-schema_timeevolution>`.
You can use the controls to adjust model parameter, start and end time, as well as the range and logarithmic scales. When you're done adjusting the model,
simply click on **Add to Simulation**. This will open a new dialog.

.. image:: /_static/7_add_dialog.png
        :align: center
        :width: 55%

In this dialog you are able to select an existing *Simulation Experiment* to add the plot to, you just created, or you can select to create a new *Simulation*.
Also you have the choice to generate a CSV DataSet from the plot, whereby this can also be done later.
However after submitting the form by clicking *Add to Simulation*, a popup will open, pointing to the new/extended Simulation. For further modifications please refere to
the :ref:`editing section <ref-expeditor>`.
