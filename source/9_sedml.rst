SED-ML implementation in JWS Online
===================================

The implementation of SED-ML in JWS Online follows closely the specification of `SED-ML Level 1 Version 2 <http://sed-ml.org/>`_, but with certain restrictions and extensions.
To support the plotting of experimental data, we decided to implement the handling of such data as the *SED-ML Level 1 Version 3* draft suggest, however we do not support
the usage of `NuML <https://github.com/NuML/NuML>`_ as container format. Instead we support Microsoft Excel Spreadsheets, which follow certain `format guidlines <#guidelines-for-experimental-data>`_.

Supported SED-ML features
-------------------------

To minimize the complexity and sustain a easy-to-use user interface, we decided to limit the SED-ML support to set of standard features:

        * SBML model support
        * Attribut-Change support
        * UniformTimeCourse simulations
        * fully support for calculations in DataGenerator
        * plotting multiple 2D plots
        * combining curves from different sources in one plot
        * DataOutputs (e.g. CSV output of simulatios)

Unsupported SED-ML features
---------------------------

        * all kind of XmlChanges
        * NuML support for experimental data
        * SteadyState simulations
        * Repeated Tasks
        * 3D Plots
        
Guidelines for experimental data
--------------------------------

Experimental data must be stored as Microsoft Excel Spreadsheet, with following requirements:

        * Data must be on the first sheet
        * First line contains the name of the column
        * Second line states the unit of the column (currently not evaluated)
        * The data is supposed to start at the third line
        * All numbers must be encoded according to the wrt number format (any cell formatted as number is parsed)

Further each file is suposed to be stored on an external server, preferebly on `Fairdom Seek <https://fairdomhub.org/>`_, and needs to be accessable via HTTP(S).
Examples:

        * https://fairdomhub.org/data_files/1329/download?version=1

