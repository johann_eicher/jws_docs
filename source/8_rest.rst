.. _ref-rest:

========
REST API
========

JWS Online supports several RESTful API functions. REST URLs are preceded by
*/rest/*. Database Searching queries are viewed by default with an `API viewer
<http://jjj.bio.vu.nl/rest/models>`_, and optionally may be viewed as JSON
using the `format=json <http://jjj.bio.vu.nl/rest/models?format=json>`_ GET
parameter. Time-evolution and steady-state results are returned as JSON objects
only. Some examples follow.

Authentification
----------------

The API of JWS Online is mostly unrestricted and can be used without authentification. 
However if models or simulations are uploaded anonymously, there is no warrant for them to 
persist, as they are treated as session models/simulations.
In case persistence is needed, one may register an JWS Online user account and use these login
credentials as HTTP Basic Authentification, to access and upload models/simulations to this user profile.
E.g. by using this Mathematica code:

.. code::
        
        URLExecute["http://jjj.bio.vu.nl/rest/" , "Headers" -> {"Authorization" -> "Basic "<>ExportString["username:password" , "Base64"]} ]

Whereby ``username:password`` needs to be replaced with the actual username/password combination, separated by a ``:`` (colon)

Database Search
---------------

List all models
^^^^^^^^^^^^^^^

`http://jjj.bio.vu.nl/rest/models <http://jjj.bio.vu.nl/rest/models>`_

Search models
^^^^^^^^^^^^^

Search species
""""""""""""""
Returns models including species ID.

`http://jjj.bio.vu.nl/rest/models/species?search=\<species id\> <http://jjj.bio.vu.nl/rest/models/species?search=atp>`_

Search reactions
""""""""""""""""
Returns models including reaction ID.

`http://jjj.bio.vu.nl/rest/models/reactions?search=\<reaction id\> <http://jjj.bio.vu.nl/rest/models/species?search=pfk>`_

Model detail
^^^^^^^^^^^^

Retrieves details about a specified model.

`http://jjj.bio.vu.nl/rest/models/\<model slug\> <http://jjj.bio.vu.nl/rest/models/teusink>`_

Retrieves details about the manuscript information attached to this model (only works for curated models)

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/manuscript/ <http://jjj.bio.vu.nl/rest/models/bachmann/manuscript/>`_

Simulations using a model
^^^^^^^^^^^^^^^^^^^^^^^^^

Retrieves a list of simulation experiments, which use on particular model

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/experiments/ <http://jjj.bio.vu.nl/rest/models/bachmann/experiments/>`_

List all simulations
^^^^^^^^^^^^^^^^^^^^

`http://jjj.bio.vu.nl/rest/experiments/ <http://jjj.bio.vu.nl/rest/experiments/>`_

Filter simulations
^^^^^^^^^^^^^^^^^^

`http://jjj.bio.vu.nl/rest/experiments/?search=\<query\> <http://jjj.bio.vu.nl/rest/experiments/?search=query>`_

Simulation experiment details
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Retrieves details about a specified simulation experiment.

`http://jjj.bio.vu.nl/rest/experiments/\<slug\>/ <http://jjj.bio.vu.nl/rest/experiments/bachmann-user/>`_

Retrieves details about the manuscript information attached to this simulation experiment (only works for curated simulations)

`http://jjj.bio.vu.nl/rest/experiments/\<slug\>/manuscript <http://jjj.bio.vu.nl/rest/experiments/bachmann-user/manuscript>`_


Model Analysis
--------------

Time evolution
^^^^^^^^^^^^^^

Perform a time evolution of specified model with default parameter values and
initial amounts. By default all species, reactions, and assignment rules are
returned.

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/time_evolution <http://jjj.bio.vu.nl/rest/models/teusink/time_evolution>`_

specifying returned entities
""""""""""""""""""""""""""""

Returned entities may be specified using the *species*, *rates*, or
*assignments* parameters. Specifying any of these will suppress the output of
all other entities. Specifying 'all' will return all entities within the class.
Individual entities may be specified as comma-separated values.  For example,
the following URL will return a time evolution result of all species:

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/time_evolution?species=all <http://jjj.bio.vu.nl/rest/models/teusink/time_evolution?species=all>`_

All rates and certain species:

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/time_evolution?rates=all;species=\<species 1\>,\<species 2\> <http://jjj.bio.vu.nl/rest/models/teusink/time_evolution?rates=all;species=BPG,NAD>`_

specifying initial amounts
""""""""""""""""""""""""""

Initial amounts may be specified using individual species IDs:

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/time_evolution?\<species 1\>=\<amount\> <http://jjj.bio.vu.nl/rest/models/teusink/time_evolution?NAD=10>`_


specifying parameter values
"""""""""""""""""""""""""""

Parameter values may be specified using individual parameter IDs:

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/time_evolution?\<parameter 1\>=\<value\> <http://jjj.bio.vu.nl/rest/models/teusink/time_evolution?gR=1.0>`_


Steady state
^^^^^^^^^^^^

Perform a steady state analysis of specified model with default parameter
values and initial amounts. Steady state fluxes and species values are
returned.

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/steady_state <http://jjj.bio.vu.nl/rest/models/teusink/steady_state>`_

specifying initial amounts
""""""""""""""""""""""""""

Initial amounts may be specified using individual species IDs:

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/steady_state?\<species 1\>=\<amount\> <http://jjj.bio.vu.nl/rest/models/teusink/steady_state?NAD=10>`_


specifying parameter values
"""""""""""""""""""""""""""

Parameter values may be specified using individual parameter IDs:

`http://jjj.bio.vu.nl/rest/models/\<model slug\>/steady_state?\<parameter 1\>=\<value\> <http://jjj.bio.vu.nl/rest/models/teusink/steady_state?gR=1.0>`_



Model Export
------------

JWS Online models may be exported in various formats including:

SBML
^^^^ 
`http://jjj.bio.vu.nl/rest/models/\<model slug\>/sbml <http://jjj.bio.vu.nl/rest/models/teusink/sbml>`_

JWS .dat
^^^^^^^^ 
`http://jjj.bio.vu.nl/rest/models/\<model slug\>/jws <http://jjj.bio.vu.nl/rest/models/teusink/jws>`_

PySCeS
^^^^^^
*under development*
`http://jjj.bio.vu.nl/rest/models/\<model slug\>/psc <http://jjj.bio.vu.nl/rest/models/teusink/psc>`_

Mathematica Notebook
^^^^^^^^^^^^^^^^^^^^
`http://jjj.bio.vu.nl/rest/models/\<model slug\>/nb <http://jjj.bio.vu.nl/rest/models/teusink/nb>`_

Modellica
^^^^^^^^^
*coming soon*


Simulation Experiment Export
----------------------------

Export as SED-ML
^^^^^^^^^^^^^^^^

This endpoint exports the simulation experiment as SED-ML file. The model links are pointing to the SBML export of the models in JWS Online.

`http://jjj.bio.vu.nl/rest/experiments/\<slug\>/export/sedml <http://jjj.bio.vu.nl/rest/experiments/bachmann-user/export/sedml>`_

Export as COMBINE Archive
^^^^^^^^^^^^^^^^^^^^^^^^^

This endpoint exports the simulation and all referenced models as `COMBINE archive <http://co.mbine.org/standards/omex>`_, so all information to reproduce this simulation is included.

`http://jjj.bio.vu.nl/rest/experiments/\<slug\>/export/combinearchive <http://jjj.bio.vu.nl/rest/experiments/bachmann-user/export/combinearchive>`_

Model and Simulation Upload
---------------------------

Upload Models and Simulations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This endpoint enables 3rd party application to directly upload a Model, a Simulation Experiment or even a COMBINE archive to JWS Online.
When successfully imported, the model or simulation will be added to the current logged in profile.
The request shall be send as `HTTP PUT` and needs to contain 2 Multipart fields:

**format**:
        Specifies the format of the uploaded file:
        //sbml, dat, sedml, combinearchive//
**file**:
        Multipart encoded file.
**constraint_model**:
        optional parameter, when uploading SBML Models. Should be set to `true` if the model should be treated as a stoichiometric model with flux constraints.

`http://jjj.bio.vu.nl/rest/upload/ <http://jjj.bio.vu.nl/rest/upload/>`_


Fetching Models and Simulations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To ease the linking with other web applications JWS Online provides an endpoint to fetch a Model or Simulation Experiment from another remote location via HTTP(S).
This might be especially usefull for 'View in JWS Online'-links. The user can simply click on the link and JWS Online imports the resource to the currently logged in user,
without messing with login credentials. Said link is a simple URL with query parameter:

**type**:
        Format of the resource to fetch:
        //sbml, dat, sedml, combinearchive//
**remote**:
        URL to the remote location.
**redirect**:
        If set to true, the user will be redirected to a user interface, showing the imported Model and Simulation Experiments. Otherwise a JSON encoded list of the result is returned.
**constraint_model**:
        Optional parameter, when uploading SBML Models. Should be set to `true` if the model should be treated as a stoichiometric model with flux constraints.

`http://jjj.bio.vu.nl/rest/fetch/?type={type}&redirect={redirect}&remote={remote} <http://jjj.bio.vu.nl/rest/fetch/?type=combinearchive&redirect=true&remote=http://scripts.sems.uni-rostock.de/getshowcase.php>`_
