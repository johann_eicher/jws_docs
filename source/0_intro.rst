.. _ref-intro:

============
Introduction
============

Welcome to the online documentation for JWS Online. The goal of this documentation is to provide an overview of the usage of all the functions available on JWS Online. 

Browser compatibility
---------------------

JWS Online has been optimised and tested on the following browsers:

    * Google Chrome
    * Chromium
    * Safari
    * Opera

Certain browsers are known to produce erroneous behaviour with JWS Online in ways that are largely outside of our control:

    * Firefox: 
        * SVGs are not rendered correctly
        * mouse gestures are over-interpreted
