.. _ref-detail:

===============
Editing a model
===============

After locating a model in the :ref:`model database section <ref-loadmodel>`, clicking on the model name (or *slug*) will automatically redirect you to the *Model Detail* page. Uploading a model, or clicking *Detail* in the simulation page will also redirect to this page. The model is divided into sections which can be accessed in by expanding the accordion sections directly below the basic *Model* section. Quick links to these sections can be accessed in along the leftand side. SBML validation errors and warning appear along the righthand side. You will not be allowed to simulate the model if there are any unaddressed SBML errors (warnings can be disregarded).

.. image:: /_static/3_detail.png

Clicking the *Simulate* button will open the model in the :ref:`Simulation Page <ref-schema>`. The *Download* button allows downloading of the model in either *SBML* or *JWS* formats. Curated models in the JWS Online database are read-only. However, clicking on the *Create derivative* button will create an editable copy of the model, which will be assigned to the current user session.

Expanding any of the accordion sections will show the current data, and a set of buttons for editing these data. The *Species* and *Reactions* sections will be explored to illustrate the editing process.


.. _ref-species-edit:

Species
-------

Expanding the *Species* section displays all the current species in terms of their ID, Name, Initial Quantity, Compartment, and Fixed state:


.. image:: /_static/3_species1.png
        :align: center

A series of buttons at the top of each section allows adding and editing of species entities:
    
    * Add Species: This button initiates a dialogue box which allows the user to add a new species in terms of the SBML attributes: 
        * ID
        * Name
        * Initial Quantity
        * 'has only substance units', which determines whether the species are in amounts (true) or concentrations (false)
        * compartment
        * fixed state

    * The *Advanced Section* allows the user to associate a Systems Biology Ontology term with the species, define whether the species is constant, and to associate a *unit definition* with the species (note that :ref:`unit definitions <ref-unit-edit>` have to first be defined before they may be selected).

    .. image:: /_static/3_species2.png
        :align: center

A number of batch editing options exist:

    * The *Edit all* button will open an extended dialogue allowing the user to edit all species simultaneously.

    .. image:: /_static/3_species_editall.png
        :align: center

    * The *Edit as text* button will open a text box, allowing the user to edit all species using a predefined syntax. 

    .. image:: /_static/3_species_edittext.png
        :align: center

.. |species_buttons| image:: /_static/3_species_littlebuttons.png
    :align: middle

.. |species_button1| image:: /_static/3_species_lb_1.png
    :align: middle

.. |species_button2| image:: /_static/3_species_lb_2.png
    :align: middle

.. |species_button3| image:: /_static/3_species_lb_3.png
    :align: middle

Finally, a convenient set of buttons exist on each species line for easy editing: |species_buttons|

    * The |species_button1| button opens the standard editing dialogue
    * The |species_button3| button deletes the species
    * The |species_button2| button opens an annotation dialogue which allows the user to assign MIRIAM annotations to species objects using the standard prepositions (is, encodes, etc.):

    .. image:: /_static/3_species_annotation.png
        :align: center

.. _ref-inits-edit:

Initial assignments
-------------------

Adding *intial assignments* to *species* are the orthodox way of setting complex initial values (i.e. when initial values are composed of combinations of previously defined parameters, concentrations etc.). Clicking the *Add initial assignment* button will display the following dialogue:

 .. image:: /_static/3_initialass.png
     :align: center

To create an *initial assignment*, select the *symbol* to from the dropdown list, and specify the *assignment* formula in the text box. In the example above, the *F6P* species has an *assignment* of :math:`3.0 \frac{KSUCC}{ATP}`. *Assignments* are evaluated recursively. Thus if *KSUCC* in the example were the subject of an *assignment rule* it would be expanded with the value of the rule's *math* element.

.. _ref-reactions-edit:

Reactions
---------

Expanding the *Reactions* section will display all the reactions in terms of their IDs, names, objective coefficients, reaction stoichiometries, kinetic laws, and flux bounds. Additionally a set of buttons will be presented for adding and editing reactions, either individually or in batch:

.. image:: /_static/3_reactions.png
    :align: center

Clicking the *Add reaction* button will display a dialogue box for creating a reactions. The reaction attributes ID, name, rate equation (a stoichiometric representation of the reaction, e.g. {1}FBP = {1}DHAP + {1}GAP), and the rate expression (or kinetic law) can be set. Additionally, a modifiers section allows the user to specify allosteric modifiers for the reaction by selecting them from a drop-down field. Clicking *Save* will create the new reaction.

.. image:: /_static/3_reactions_add.png
    :align: center

The *edit* buttons function similarly to the the *Species* edit buttons :ref:`mentioned previously <ref-species-edit>`. Clicking the |species_button1| button will display an edit dialogue which is similar to the *Add reaction* dialogue, with the fields populated using the current reaction's attributes. Additionally, if the model is a *constraint-based model*, flux bounds and objectives may be set.

.. image:: /_static/3_reactions_edit.png
    :align: center

* The *Edit all* button allows the user to edit all reactions in an extended dialogue:

 .. image:: /_static/3_reactions_edit_all.png
     :align: center

* The *Edit reactions as text* button displays an text dialogue allowing the user to edit, add, or remove reaction stoichiometries simultaneously using a simple syntax:

 .. image:: /_static/3_reactions_edit_text.png
     :align: center

* Similarly the *Edit kinetic laws as text* button allows the user to edit all kinetic laws in a text box; each kinetic law being represented as a simple equation on a new line:

 .. image:: /_static/3_reactions_edit_kinlaws.png
     :align: center


.. _ref-rules-edit:

Rules
-----

Assignment rules, rate rules, and algebraic rules can be added or edited in the *Rules* section. 

 .. image:: /_static/3_rules.png
     :align: center

Rules may be added by clicking the appropriate *Add _ Rule* button. 

 .. image:: /_static/3_rules_add.png
     :align: center


.. _ref-unit-edit:

Units
-----

Unit definitions must be created before they can be assigned to an entity. Clicking the *Add Unit Definition* button will display the following dialogue:

 .. image:: /_static/3_units.png
     :align: center

As mentioned in the dialogue box, the unit definition will be composed of the product of a set of units. Each unit is composed of a multiplier, a scale, a kind, and finally an exponent. The *kind* dropdown contains the major kinds of unit used in dynamic models. For example, to create a *mM* (:math:`\frac{mmol}{l} = \frac{mole \times 10^{-3}}{l}`) unit definition, first create a *mmol* unit by selecting *mole* in *kind*, and setting the *multiplier* to 1, setting the *scale* to -3, and finally setting the *exponent* to 1 (i.e. :math:`1 \times 10^{-3} moles`). To create a *litre* unit, in the following row set *multiplier* to 1, *scale* to 0, select *litre* from *kind*, and set the *exponent* to 1.

 .. image:: /_static/3_units_mM.png
     :align: center

After naming the unit definition and saving, it will be available to be assigned to model entities. Note that in some browsers it is necessary to refresh the page after saving the unit definition for it to be visible.


.. _ref-events-edit:

Events
------

Conditional events may be added in the *events* section. Clicking *Add Event* will display the following dialogue:

 .. image:: /_static/3_events.png
     :align: center

To create an event, set the *trigger* using comparative operators (e.g. time > 10, ATP = 4), then select the *variable* and specify the *assignment* value for the *variable* if the *trigger* conditions are met. The *assignment* may contain symbolic elements (e.g. parameters, species).

