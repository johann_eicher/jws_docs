.. _ref-builder:

================
Building a model
================

For the most part, building a model entirely in JWS follows the same procedural steps as outlined in the :ref:`Editing a model <ref-detail>` section. Though there is no correct order in which to construct a model, we have found that the following approach works well:

1. Clicking on the *Build model* link in the navigation bar will prompt you for a short ID and an optional model name. After saving these two values, you will be redirected to the familiar *model detail page*.
2. If required, create :ref:`Unit definitions <ref-unit-edit>` first so that you will not need to return to all the model objects and assign units after model creation. This is the last section in the accordion.
3. Next, define the *compartments*. All *species* must be assigned a compartment upon creation. 
4. :ref:`Species <ref-species-edit>`, parameters, :ref:`initial assignments <ref-inits-edit>`, *functions* and any :ref:`rules <ref-rules-edit>` should now be defined.
5. :ref:`Reactions <ref-reactions-edit>` should be defined next, as all the necessary *species*, *functions* and *parameters* (as well as rules) now exist in the database. 
6. Finally, define :ref:`events <ref-events-edit>`.
