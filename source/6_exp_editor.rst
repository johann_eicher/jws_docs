
.. _ref-expeditor:

.. |button_eye| image:: /_static/6_button_eye.png
        :align: middle
.. |button_wrench| image:: /_static/6_button_wrench.png
        :align: middle
.. |button_pen| image:: /_static/6_button_pen.png
        :align: middle
.. |button_delete| image:: /_static/6_button_delete.png
        :align: middle


Editing a Simulation Experiment
===============================

After locating the Simulation Experiment of interest, clicking on the name (or slug) will redirect you to the *Simulation Detail* page. The page is divided into sections, which can be easily accessed by expanding the accordions.
In SED-ML each main element has an assigned ID. In JWS the user is supposed to set sane values for these, but must follow two rules:

        * Each ID must be unique within the Simulation Experiment
        * An ID is only allowed to contain
                * letters (upper and lower case, case-sensitive)
                * numbers
                * underscores

.. image:: /_static/6_editor.png
        :align: center
        :width: 80%

Models
------

The *Models* section lists all referenced models of this Simulation Experiment in terms of their ID and name in regards of the SED-ML reference and a link to the actual model in JWS Online.

.. image:: /_static/6_models.png
        :align: center
.. |models_buttons| image:: /_static/6_models_buttons.png
        :align: middle

Each model reference comes with a set of  buttons |models_buttons|, allowing you to edit |button_pen| or delete |button_delete| the reference, as well as to view |button_eye| and adjust |button_wrench| attribute changes (e.g. *Parameters*, *Initial Assignments*, etc.)
If a model reference has no attribute changes, the default values from the model are taken. Currently JWS Online does not support more complicated changes like XML insert/delete, due to the database structure. Therefore it is advised to create a derivative of the model and apply the intended changes directly.

.. image:: /_static/6_models_changes.png
        :align: center
        :width: 80%
        
Data Descriptions
-----------------

Expanding the *Data Descriptions* section shows a list of data descriptions, referenced in this Simulation Experiment and linking to Excel spreadsheet in `Fairdom Seek <https://fairdomhub.org/>`_. The spreadsheet is supposed to followed `specific guidelines <9_sedml.html#guidelines-for-experimental-data>`_.
        
.. image:: /_static/6_data.png
        :align: center

In addition to these format restrictions it is necessary to create a mapping between the column names of the Spreadsheet and an SED-ML ID, so the columns can be referenced later in the `Plotting Section <#plots>`_.
These mappings are called *Data Sources* and can be edited by clicking on |button_pen| or viewed by clicking on |button_eye|.
Whereby the *index set* defines the name of the column and ID is the SED-ML ID, to used for referencing

.. image:: /_static/6_data_edit.png
        :align: center
        :width: 80%

Simulation Tasks
----------------

The *Simulation Tasks* section holds the actual settings for simulating the models, by combining two SED-ML concepts: Simulations and Tasks. This results in shorter work-flow, eliminating one additional step.
Editing |button_pen| and deleting |button_delete| are easily performed by clicking on the buttons in each row.

.. image:: /_static/6_simtask.png
        :align: center
        :width: 80%

Opening the edit dialog allows for the adjustment of multiple parameter:
        * **Id:** the SED-ML ID of this simulation task
        * **Name:** a human readable name of this task. Main use case is documentation
        * **Model:** the link to a model referenced in the `Models <#models>`_ section of this Simulation Experiment
        * **Inital Time:** the start time of the simulation (t_0)
        * **Output Start Time:** the first time point to be plotted
          (is omitted for simulations performed in JWS Online)
        * **Output End Time:** the end time of the simulation
        * **Number of Points:** the amount of data point to be used for plotting
          (is omitted for simulations performed in JWS Online)
        * **Kisao Id:** the type of simulation to be performed according to the `KiSAO Ontology <http://co.mbine.org/standards/kisao>`_

Both **Output Start Time** and **Kisao Id*** are ignored in JWS Online, but kept in the edit dialog for compatibility when exporting the Simulation Experiment.

.. image:: /_static/6_simtask_edit.png
        :align: center
        :width: 80%

Plots
-----
.. _ref-plots:

The *Plot* section defines the final output of the Simulation Experiment. It consists of different plots, where each plot can contain multiple curves. The settings and curve definitions for each plot can be viewed |button_eye|, edited |button_pen| and deleted |button_delete| by clicking on the buttons in each row.

.. image:: /_static/6_plot.png
        :align: center
        :width: 80%

The edit dialog is divided into two separate sections: In the upper one, the settings for the plot itself are defined: SED-ML ID and name. Whereby the name defines the title of this plot.
In the lower one are the curves defined, whereby each row in the table defines one curve. Available settings are:

        * **Id:** the SED-ML ID of this plot
        * **Name:** the name for this plot, used in the legend
        * **Log X:** defines if the x axis should be in log scales
          (is omitted for simulations performed in JWS Online, kept for compatibility to SED-ML)
        * **X Data:** the calculation performed for the x data of this curve in `Infix Notation <#plot-infix-notation>>`_
        * **Log Y:** defines if the y axis should be in log scales
        * **Y Data:** the calculation performed for the y data of this curve in `Infix Notation <#plot-infix-notation>`_

.. image:: /_static/6_plot_edit.png
        :align: center
        :width: 80%

Plot Infix Notation
^^^^^^^^^^^^^^^^^^^

To apply post processing to simulation results or data JWS Online uses standard infix notation. 
Next to a set of default operations and functions defined in the `SBML Level 1 Version 2 <http://sbml.org/Special/specifications/sbml-level-1/version-2/html/sbml-level-1.html#SECTION00035000000000000000>`_ standard,
JWS Online defines a set of custom function to reference results from model simulations and experimental data. These functions are:

**data(data_set_id, data_id)**
        Refers to a `data <#data-descriptions>`_ source (column) of experimental data. the *data_id* must be an ID of a `Data Description <#data-descriptions>`_.

**specie(task_id, specie_id)**
        Refers to a species from a simulated model. The *task_id* must be an ID of a `Simulation Task <#simulation-task>`_ not a model or model reference.

**reaction(task_id, reaction_id)**
        Refers to a reaction from a simulated model. The *task_id* must be an ID of a `Simulation Task <#simulation-task>`_ not a model or model reference.

**compartment(task_id, comp_id)**
        Refers to a compartment from a simulated model. The *task_id* must be an ID of a `Simulation Task <#simulation-task>`_ not a model or model reference.

**param(task_id, param_id)**
        Refers to a parameter from a model. The *task_id* must be an ID of a `Simulation Task <#simulation-task>`_ not a model or model reference.

**symbol(task_id, symbol)**
        Refers to a symbol from a model, like *time*. The *task_id* must be an ID of a `Simulation Task <#simulation-task>`_ not a model or model reference.

**const(const_name, value)**
        Defines a constant in the equation. Despite just using a a plain floating point number in the equation, this function allows you to define a name for the constant (which needs to follow the rules for IDs)
        Also the constant gets converted into Parameter in the DataGenerator, when exporting as SED-ML, which is recommended by specification.
